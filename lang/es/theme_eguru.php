<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_eguru', language 'en'
 *
 * @package   theme_eguru
 * @copyright 2015 LMSACE Dev Team, lmsace.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['about'] = 'Acerca de';
$string['aboutus'] = 'Acerca de nosotros';
$string['pluginname'] = 'Eguru';
$string['address'] = 'Dirección';
$string['choosereadme'] = '<div class="clearfix"><div class="theme_screenshot">
<img class=img-polaroid src="eguru/pix/screenshot.jpg" />
<h3>Theme Credits</h3>
<p>
<h3>Moodle Eguru theme</h3>
<p>
This theme is based on the Bootstrapbase Moodle theme.
</p>
<p>
<a href="http://getbootstrap.com">http://getbootstrap.com</a>
</p>
<p>
Authors: LMSACE Dev Team<br>
Contact: info@lmsace.com<br>
Website: <a href="http://www.lmsace.com">www.lmsace.com</a><br>
</p>';
$string['configtitle'] = 'Eguru';
$string['connectus'] = 'Conectate con nosotros';
$string['contact'] = 'Contacto';
$string['customcss'] = 'css personalizado';
$string['customcssdesc'] = 'Sea cual sea CSS reglas se agrega a esta área de texto se reflejará en cada página, para hacer más fácil la personalización de este tema.';
$string['defaultaddress'] = '308 Negra Narrow Lane, Albeeze, New york, 87104';
$string['defaultemailid'] = 'info@example.com';
$string['defaultphoneno'] = '(000) 123-456';
$string['emailid'] = 'Email';
$string['fburl'] = 'Facebook';
$string['fburl_default'] = 'https://www.facebook.com/yourfacebookid';
$string['fburldesc'] = 'La url de Facebook de su organización.';
$string['pcourses'] = 'Cursos promocionados';
$string['pcoursesdesc'] = 'Por favor dar el id cursos promovidos debe separados por comas.';
$string['promotedcoursesheading'] = 'Cursos promocionados';
$string['footerheading'] = 'Bloques de pie de página';
$string['footnote'] = 'Nota al pie';
$string['footnotedefault'] = 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos.';
$string['footnotedesc'] = 'Lo que se agrega a esta área de texto se mostrará en el pie a través de su Moodle site.Please dan la clave sea la lengua o text.For ex: lang:display or Display';
$string['frontpageheading'] = 'primera plana';
$string['gpurl'] = 'Google+';
$string['gpurl_default'] = 'https://www.google.com/+yourgoogleplusid';
$string['gpurldesc'] = 'La url Google+ de su organización.';
$string['headerheading'] = 'Cabecera';
$string['logo'] = 'logotipo';
$string['logodesc'] = 'Por favor, cargue su logotipo personalizado aquí si quieres añadirlo a la cabecera.
                       <br>La imagen debe ser 50px alto y cualquier anchura razonable (mínimo: 235px) que trajes.';
$string['numberofslides'] = 'Número de diapositivas';
$string['numberofslides_desc'] = 'Número de diapositivas sobre el control deslizante.';
$string['numberoftmonials'] = 'Número de Testimonios.';
$string['numberoftmonials_desc'] = 'Número de Testimonios en la Página de Inicio.';
$string['phoneno'] = 'Telefono no';
$string['pinurl'] = 'Pinterest';
$string['pinurl_default'] = 'https://in.pinterest.com/yourpinterestname/';
$string['pinurldesc'] = 'La url Pinterest de su organización.';
$string['knowmore'] = 'Saber más';
$string['region-side-post'] = 'Correcto';
$string['region-side-pre'] = 'Izquierda';
$string['signup'] = 'contratar';
$string['slidecaption'] = 'leyenda de diapositivas';
$string['slidecaptiondefault'] = 'Moodle Learning Management System';
$string['slidecaptiondesc'] = 'Introduzca el texto de la leyenda que se utilizará para la diapositiva';
$string['slideimage'] = 'imagen Diapositiva';
$string['slideimagedesc'] = 'La imagen debe ser 1366px X 385px.';
$string['slideno'] = 'Diapositiva {$a->slide}';
$string['slidenodesc'] = 'Introduzca los ajustes de diapositivas {$a->slide}.';
$string['slideshowdesc'] = 'Esto crea una presentación de diapositivas de hasta doce diapositivas para usted para promover elementos importantes de su sitio. El espectáculo es sensible en altura de la imagen se ajusta de acuerdo a la pantalla tamaño.Si se selecciona ninguna imagen para una diapositiva, entonces se utiliza las imágenes default_slide en la carpeta pix.';
$string['slideshowheading'] = 'Página de inicio deslizante.';
$string['slideshowheadingsub'] = 'Presentación de la primera página.';
$string['slideurl'] = 'enlace de diapositivas.';
$string['slideurldesc'] = 'Introduzca el destino objetivo del vínculo de la imagen de diapositivas.';
$string['toggleslideshow'] = 'Presentación de diapositivas pantalla.';
$string['toggleslideshowdesc'] = 'Elija si desea ocultar o mostrar la presentación de diapositivas.';
$string['twurl'] = 'Twitter';
$string['twurl_default'] = 'https://twitter.com/yourtwittername';
$string['twurldesc'] = 'La url de Twitter de su organización.';
$string['themegeneralsettings'] = 'General';
$string['patternselect'] = 'Esquema de color del sitio';
$string['patternselectdesc'] = 'Seleccione el esquema de color que desea tener para su sitio.';
$string['colorscheme'] = 'Esquema de colores';
$string['footerblock'] = 'pie de página Bloquear';
$string['title'] = 'Título';
$string['footerbtitle_desc'] = 'Por favor, dar el título o bien tecla de idioma bloque pie de página o text.For ex: lang:display or Display';
$string['footerbtitle2default'] = 'Enlaces rápidos';
$string['footerbtitle3default'] = 'Síganos';
$string['footerbtitle4default'] = 'Contacto';
$string['footerblink'] = 'Pie de página Bloquear Enlace';
$string['footerblink2default'] = 'lang:aboutus|http://www.example.com/about-us.php
lang:termsofuse|http://www.example.com/terms-of-use.php
lang:faq|http://www.example.com/faq.php
lang:support|http://www.example.com/support.php
lang:contact|http://www.example.com/contact.php';
$string['footerblink_desc'] = 'Puede configurar un pie de página Bloquear Links aquí para ser mostrado por temas. Cada línea se compone de un texto del menú o bien tecla de idioma o el texto, un link URL (opcional), separados por tubería characters.For ejemplo:
<pre>
lang:moodlecommunity|https://moodle.org
Moodle Support|https://moodle.org/support
</pre>';
$string['medianame1'] = 'Facebook';
$string['medianame2'] = 'Twitter';
$string['medianame3'] = 'Google Plus';
$string['medianame4'] = 'Pinterest';

$string['mediaicon1'] = 'fa-facebook-f';
$string['mediaicon2'] = 'fa-twitter';
$string['mediaicon3'] = 'fa-google-plus';
$string['mediaicon4'] = 'fa-pinterest-p';
$string['default_color'] = 'Esquema de color por defecto';
$string['color_schemes_heading'] = 'Combinaciones de colores';
$string['promotedtitledefault'] = 'cursos promocionados';
$string['promotedtitledesc'] = 'Por favor, dar el título de bloque cursos promovidos, cualquiera de las teclas idioma o Text.
For ex: lang:display or Display';
$string['pcourseenable'] = 'Habilitar cursos promocionados';
$string['login'] = 'Iniciar sesión';
$string['link'] = 'vínculo';
$string['text'] = 'Texto';
$string['icon'] = 'Icono';
$string['loginheader'] = 'Accede a tu cuenta';
$string['marketingspotsheading'] = 'manchas de marketing';
$string['marketingspot'] = 'punto de comercialización';
$string['marketingspot1desc'] = 'Introduzca el 1 bloque de contenido, ya sea tecla de idioma de Marketing del punto o texto here.For ex: lang:display or Display';
$string['marketingspot2desc'] = 'Introduzca el 2 bloque de contenido, ya sea tecla de idioma de Marketing del punto o texto here.For ex: lang:display or Display';
$string['mspottitledesc'] = 'Introduzca el marketing Punto Título cualquier tecla de idioma o Text.For ex: lang:display or Display';
$string['mspotdescdesc'] = 'Introduzca el marketing Descripción del punto ya sea lenguaje clave o Text.For ex: lang:display or Display';
$string['mspot1titledefault'] = 'plurilingüe';
$string['mspot1descdefault'] = 'Esta es la versión de Photoshop de Lorem Ipsum. Proin gravida nibh vel Velit auctor aliquet. Sollicitudin Aenean, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot2titledefault'] = 'Estudio en línea';
$string['mspot2descdefault'] = 'TEsta es la versión de Photoshop de Lorem Ipsum. Proin gravida nibh vel Velit auctor aliquet. Sollicitudin Aenean, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot3titledefault'] = 'Apoyo a la Comunidad';
$string['mspot3descdefault'] = 'Esta es la versión de Photoshop de Lorem Ipsum. Proin gravida nibh vel Velit auctor aliquet. Sollicitudin Aenean, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot4titledefault'] = 'Diseño de respuesta';
$string['mspot4descdefault'] = 'Esta es la versión de Photoshop de Lorem Ipsum. Proin gravida nibh vel Velit auctor aliquet. Sollicitudin Aenean, lorem quis bibendum auctor, nisi elit consequat.';
$string['faicondesc'] = 'Introduzca el nombre del icono que desea utilizar. Lista es <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target="_new">here</a>. Solo tienes que introducir lo que es después de la "fa-".';
$string['slideurltext'] = 'Slide link text';
$string['slideurltextdesc'] = 'Introduzca el destino objetivo del texto deslizante enlace de imagen,
cualquiera de las teclas idioma o Text.For ex: lang:display or Display';
$string['termsofuse'] = 'Condiciones de uso';
$string['faq'] = 'PF';
$string['support'] = 'apoyo';
$string['copyright'] = '<p class = "text-center"> Derechos de autor y copia; 2015 - Desarrollado por
             <a href="http://www.lmsace.com/"> LMSACE.com </a> .Powered por <a href="https://moodle.org"> Moodle </a> </ p>';
$string['footeremail'] = 'Correo:';